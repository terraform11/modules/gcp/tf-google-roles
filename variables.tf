variable "role_id" {
  description = "id to role"
}

variable "role_title" {
  description = "A human-readable title for the role."
}

variable "role_permissions" {
  description = "The names of the permissions this role grants when bound in an IAM policy. At least one permission must be specified."
  type        = set(string)
}

variable "project" {
  description = "The project that the service account will be created in."
}


variable "role_stage" {
  description = "The current launch stage of the role. Defaults to GA."
  default     = "GA"
}

variable "role_description" {
  description = "A human-readable description for the role."
}