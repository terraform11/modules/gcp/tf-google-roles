resource "google_project_iam_custom_role" "custom_role" {
    role_id = var.role_id
    title = var.role_title
    permissions = var.role_permissions
    project     = var.project
    stage       = var.role_stage
    description = var.role_description

}
